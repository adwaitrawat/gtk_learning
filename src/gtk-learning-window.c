/* gtk_learning-window.c
 *
 * Copyright 2019 Adwait Rawat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gtk-learning-config.h"
#include "gtk-learning-window.h"

struct _GtkLearningWindow
{
  GtkApplicationWindow  parent_instance;

  GtkHeaderBar        *header_bar;
  GtkTextView         *text_view_get;
  GtkTextView         *text_view_set;
  GtkButton           *get_button;
  GtkButton           *set_button;
};

G_DEFINE_TYPE (GtkLearningWindow, gtk_learning_window, GTK_TYPE_APPLICATION_WINDOW)

static gint multiple;

static void
get_button_cb (GtkButton *get_button, GtkLearningWindow *window)
{
  g_assert (GTK_IS_BUTTON (get_button));
  g_assert (GTK_IS_WINDOW (window));

  GtkTextBuffer* buffer = NULL;
  GtkTextIter begin;
  GtkTextIter end;
  gchar* text = NULL;
  gchar* endptr = NULL;

  multiple *= 2;
  printf ("%d\n", multiple);
  buffer = gtk_text_view_get_buffer (window->text_view_get);
  gtk_text_buffer_get_bounds (buffer, &begin, &end);
  text = gtk_text_buffer_get_text (buffer, &begin, &end, FALSE);
  if (g_ascii_isdigit (*text))
  {
    printf ("IS DIGIT\n");
    multiple = g_ascii_strtoll (text, &endptr, 10);
    printf ("%d\n", multiple);
  }
  else
  {
    printf ("%s\n", text);
  }
}

static void
set_button_cb (GtkButton *set_button, GtkLearningWindow *window)
{
  g_assert (GTK_IS_BUTTON (set_button));
  g_assert (GTK_IS_WINDOW (window));

  GtkTextBuffer* buffer = gtk_text_buffer_new (NULL);

  printf ("%d\n", multiple);
  gchar *text = g_strdup_printf ("%d", multiple);
  gtk_text_buffer_set_text (buffer, text, -1);
  gtk_text_view_set_buffer (window->text_view_set, buffer);
}

static void
gtk_learning_window_class_init (GtkLearningWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  multiple = 0;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gtk_learning/App/gtk-learning-window.ui");
  gtk_widget_class_bind_template_child (widget_class, GtkLearningWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, GtkLearningWindow, text_view_get);
  gtk_widget_class_bind_template_child (widget_class, GtkLearningWindow, text_view_set);
  gtk_widget_class_bind_template_child (widget_class, GtkLearningWindow, get_button);
  gtk_widget_class_bind_template_child (widget_class, GtkLearningWindow, set_button);
}

static void
gtk_learning_window_init (GtkLearningWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
  gtk_widget_set_can_focus (GTK_WIDGET (self), TRUE);

  g_signal_connect (self->get_button, "pressed", G_CALLBACK (get_button_cb), self);
  g_signal_connect (self->set_button, "pressed", G_CALLBACK (set_button_cb), self);
}
